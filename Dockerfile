FROM rust:latest AS builder

WORKDIR /app

COPY . .

RUN cargo build --release 

FROM debian:12

WORKDIR /app

RUN apt update && apt install -y rsync

COPY --from=builder /app/target/release/volcache /usr/bin/volcache

ENV RUST_LOG=info

CMD ["volcache"]
