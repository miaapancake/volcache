# Volcache
![Drone (self-hosted)](https://img.shields.io/drone/build/miaapancake/volcache?server=https%3A%2F%2Fci.miadoes.dev%2F&style=for-the-badge&logo=drone)
![Docker Pulls](https://img.shields.io/docker/pulls/miadoesdev/volcache?style=for-the-badge)
![Static Badge](https://img.shields.io/badge/License-MIT-C577F9?style=for-the-badge)

Thread Safe :white_check_mark: Volume Based :cd: Cache For DroneCI


## Options
|name    |description|type |required|example|
|:------:|:---------:|:---:|:------:|:-----:|
|mount|directories to mount|`string[]`|`yes`|node_modules|
|rebuild|whether to rebuild the cache either this or restore is required|`boolean`|`no`|true|
|restore|whether to restore the cache either this or rebuild is required|`boolean`|`no`|true|
|cache_key_files|files that make up the hash that determines whether the cache will be rebuild or not|`string[]`|`no`|package-lock.yaml|
|cache_keys|arbitrary values that get represent the key for the cache|`string[]`|`no`|asdfasd|

Notes:
- If neither `cache_key_files` nor `cache_keys` is set volcache will default to the branch name

## Example configs using volcache

### Rust Pipeline

```yaml
kind: pipeline
name: my_pipeline
steps:
  - name: Restore Cache
    image: miadoesdev/volcache
    settings:
      restore: true
      mount:
        - /cargo
        - ./target
    volumes:
      - name: cache
        path: /cache 

  - name: Run Build
    image: rust:latest
    environment:
        CARGO_HOME: /cargo
    commands:
      - cargo build 

  - name: Rebuild Cache
    image: miadoesdev/volcache
    settings:
      rebuild: true
      mount:
        - /cargo
        - ./target
    volumes:
      - name: cache
        path: /cache

volumes:
  - name: cache
    host:
      path: /tmp/cache

```

### Javascript

```yaml
kind: pipeline
name: my_pipeline
steps:
  - name: Restore Cache
    image: miadoesdev/volcache
    settings:
      cache_key_files:
        - package-lock.json
      restore: true
      mount:
        - node_modules
    volumes:
      - name: cache
        path: /cache 

  - name: install node packages
    image: node:20-slim
    commands:
      - npm install

  - name: Rebuild Cache
    image: miadoesdev/volcache
    settings:
      cache_key_files:
        - package-lock.json
      rebuild: true
      mount:
        - node_modules
    volumes:
      - name: cache
        path: /cache

volumes:
  - name: cache
    host:
      path: /tmp/cache

```
