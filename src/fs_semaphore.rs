use std::fs;
use std::fs::OpenOptions;
use std::io;
use std::io::prelude::*;
use std::ops::Add;
use std::path::PathBuf;
use std::str::FromStr;
use std::thread::sleep;
use std::time::Duration;

use log::*;
use uuid::Uuid;

pub struct FsSemaphore {
    id: Uuid,
    path: PathBuf,
}

impl FsSemaphore {
    pub fn new(path: PathBuf) -> Self {
        Self {
            id: Uuid::new_v4(),
            path,
        }
    }

    fn get_lockfile_path(&self) -> PathBuf {
        self.path.join("cache.lock")
    }

    fn read_lockfile(&self) -> Vec<Uuid> {
        fs::read_to_string(self.get_lockfile_path())
            .expect("failed to read lockfile")
            .split('\n')
            .filter(|s| Uuid::from_str(s).is_ok())
            .map(|s| {
                Uuid::from_str(s)
                    .expect("failed to convert lockfile entry to uuid")
            })
            .collect()
    }

    fn write_lockfile(&self, queue: &[Uuid]) -> io::Result<()> {
        fs::write(
            self.get_lockfile_path(),
            queue
                .iter()
                .map(|id| id.to_string())
                .collect::<Vec<String>>()
                .join("\n")
                .add("\n"),
        )
    }

    fn wait(&self) {
        loop {
            if !self.get_lockfile_path().exists() {
                break;
            }

            let lock_contents = self.read_lockfile();
            let Some(first_uuid) = lock_contents.get(0) else {
                break;
            };

            if self.id == *first_uuid {
                break;
            }

            info!("Cache is in use waiting in queue...");

            #[cfg(not(test))]
            sleep(Duration::from_secs(5));

            #[cfg(test)]
            sleep(Duration::from_secs(1));
        }
        info!(
            "My turn! {} is now controlling lockfile",
            self.id.to_string()
        );
    }

    pub fn lock(&self) {
        if !self.path.exists() {
            fs::create_dir_all(&self.path).unwrap_or_else(|_| {
                panic!("failed to ensure: {}", self.path.to_string_lossy())
            });
        }

        {
            let mut file_handle = OpenOptions::new()
                .write(true)
                .append(true)
                .create(true)
                .open(self.get_lockfile_path())
                .expect("failed to open lockfile");

            writeln!(file_handle, "{}", self.id)
                .expect("failed to write to lockfile");
        }

        self.wait();
    }
}

impl Drop for FsSemaphore {
    fn drop(&mut self) {
        info!("Releasing lockfile from {}", self.id);
        let mut lock_contents = self.read_lockfile();

        if lock_contents.len() <= 1 {
            fs::remove_file(self.get_lockfile_path())
                .expect("failed to remove lockfile");
        } else {
            lock_contents.remove(0);
            self.write_lockfile(&lock_contents)
                .expect("failed to modify lockfile queue");
        }
    }
}
