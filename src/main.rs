use std::fs;
use std::path::{Path, PathBuf};
use std::process::Command;

use jealousy::FromEnv;
use log::*;
use serde::Deserialize;

use crate::fs_semaphore::FsSemaphore;

mod fs_semaphore;

#[derive(Deserialize, FromEnv, Debug)]
#[from_env(prefix = "DRONE")]
struct DroneEnv {
    commit_branch: String,
    commit_message: String,
    repo_name: String,
    repo_owner: String,
}

#[derive(Deserialize, FromEnv, Debug)]
#[from_env(prefix = "PLUGIN")]
struct PluginEnv {
    mount: Vec<String>,
    rebuild: Option<bool>,
    restore: Option<bool>,
    cache_key_files: Option<Vec<String>>,
    cache_keys: Option<Vec<String>>,
}

impl PluginEnv {
    pub fn get_cache_path(&self, drone_env: &DroneEnv) -> PathBuf {
        let cache_path: String;

        if let Some(key_files) = self.cache_key_files.clone() {
            cache_path = key_files
                .iter()
                .map(|cache_file| match fs::read(cache_file) {
                    Ok(var) => format!("{:x}", md5::compute(var)),
                    Err(err) => {
                        panic!("failed to read file: {} {}", cache_file, err)
                    }
                })
                .collect::<Vec<String>>()
                .join("/");
        } else if let Some(keys) = self.cache_keys.clone() {
            cache_path = keys.join("/");
        } else {
            cache_path = drone_env.commit_branch.clone();
        }

        #[cfg(not(debug_assertions))]
        let out = format!(
            "/cache/{}/{}/{cache_path}",
            drone_env.repo_owner, drone_env.repo_name
        );

        #[cfg(any(debug_assertions, test))]
        let out = format!(
            "/tmp/cache/{}/{}/{cache_path}",
            drone_env.repo_owner, drone_env.repo_name
        );

        PathBuf::from(out)
    }
}

fn copy_dir(from: &str, to: &str) -> std::io::Result<()> {
    if Path::new(to).exists() {
        fs::remove_dir_all(to)?;
    }
    fs::create_dir_all(to)?;

    Command::new("rsync")
        .arg("-aHA")
        .arg("--delete")
        .arg(format!("{from}/"))
        .arg(to)
        .output()?;

    Ok(())
}

fn rebuild(source: PathBuf, cache_path: PathBuf) {
    let semaphore =
        FsSemaphore::new(cache_path.clone().parent().unwrap().to_path_buf());
    semaphore.lock();

    info!("Rebuilding from {}", source.to_string_lossy());

    copy_dir(&source.to_string_lossy(), &cache_path.to_string_lossy())
        .expect("failed to rebuild cache");
}

fn restore(source: PathBuf, cache_path: PathBuf) {
    if !cache_path.exists() {
        info!("No cache found... skipping");
        return;
    }

    info!("Cache found! restoring from cache...");

    copy_dir(&cache_path.to_string_lossy(), &source.to_string_lossy())
        .expect("failed to restore cache");
}

fn get_relative_path(path: &Path) -> &Path {
    if path.is_relative() {
        return path.clone();
    }

    path.strip_prefix("/").unwrap()
}

fn run_cache(plugin_env: &PluginEnv, drone_env: &DroneEnv) {
    let base_cache_path = plugin_env.get_cache_path(drone_env);

    for source in plugin_env.mount.iter() {
        let source = PathBuf::from(source);
        let cache_path = base_cache_path.join(get_relative_path(&source));

        match (plugin_env.rebuild, plugin_env.restore) {
            (Some(true), None | Some(false)) => rebuild(source, cache_path),
            (None | Some(false), Some(true)) => restore(source, cache_path),
            (Some(true), Some(true)) => {
                panic!("Cannot specify to both restore and rebuild")
            }
            _ => panic!("Please specify to either rebuild or restore!"),
        }
    }
}

fn main() {
    env_logger::init();

    let drone_env = DroneEnv::from_env().expect("Failed to get DRONE env");

    let plugin_env = match PluginEnv::from_env() {
        Ok(val) => val,

        // Required plugin variable is unset
        Err(jealousy::Error::MissingValue(var)) => {
            panic!("Missing the {var} property!");
        }

        // an unexpected error happened
        _ => {
            panic!("Invalid plugin enviroment!");
        }
    };

    if drone_env.commit_message.contains("[NO CACHE]") {
        info!("Found [NO CACHE] in commit message, skipping cache restore and rebuild!");
        return;
    }

    run_cache(&plugin_env, &drone_env);
}

#[cfg(test)]
mod tests {
    use std::sync::Once;

    use super::*;
    static INIT: Once = Once::new();

    /// set up the environment and logging
    fn setup_env(rebuild: bool, restore: bool) -> (DroneEnv, PluginEnv) {
        INIT.call_once(|| env_logger::init());
        let drone_env = DroneEnv {
            commit_message: String::from("I did an oopsy"),
            commit_branch: String::from("do_me_a_dirty"),
            repo_name: String::from("components"),
            repo_owner: String::from("miaapancake"),
        };
        let plugin_env = PluginEnv {
            mount: vec![
                String::from("/tmp/volcache/example/node_modules"),
                String::from("/tmp/volcache/example/more_modules"),
            ],
            cache_key_files: Some(vec![String::from("Cargo.lock")]),
            cache_keys: None,
            rebuild: Some(rebuild),
            restore: Some(restore),
        };

        (drone_env, plugin_env)
    }

    /// check if run_cache panics, if we set the plugin_env to BOTH rebuild AND restore
    #[test]
    #[should_panic]
    fn test_no_rebuild_and_restore() {
        let (drone_env, plugin_env) = setup_env(true, true);
        run_cache(&plugin_env, &drone_env);
    }

    /// check if we can successfully do a rebuild and restore cycle
    #[test]
    fn test_rebuild_and_restore() {
        // Copy the example to tmp to test out FS operations on
        copy_dir("./example", "/tmp/volcache/example")
            .expect("failed to copy example to tmp");

        // Set up env vars for testing
        let (drone_env, plugin_env) = setup_env(true, false);
        run_cache(&plugin_env, &drone_env);

        // Check if the cache has been built
        let base_cache_path = plugin_env.get_cache_path(&drone_env);
        for source in plugin_env.mount.iter() {
            let source = source.strip_prefix("/").unwrap();
            assert!(base_cache_path.join(source).exists());
        }

        // Remove the original directories
        fs::remove_dir_all("/tmp/volcache/example/")
            .expect("Failed to delete example");

        // restore cache
        let (drone_env, plugin_env) = setup_env(false, true);
        run_cache(&plugin_env, &drone_env);

        // Check if they have been restored
        for source in plugin_env.mount.iter() {
            debug!("Checking if {} exists", source);
            assert!(Path::new(source).exists());
        }
    }
}
